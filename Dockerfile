FROM cloudron/base:2.0.0 AS base

ARG GLPI_VERSION=9.5.3

# Download glpi and install dependencies
RUN mkdir -p /app/code && \
    curl -L https://github.com/glpi-project/glpi/archive/${GLPI_VERSION}.tar.gz | tar zxf - --strip-components 1 -C /app/code && \
    php /app/code/bin/console dependencies install

# Apache configuration
RUN rm /etc/apache2/sites-enabled/* && \
    sed -e 's,^ErrorLog.*,ErrorLog "|/bin/cat",' -i /etc/apache2/apache2.conf && \
    a2disconf other-vhosts-access-log && \
    # Listen on port 3000
    echo "Listen 3000" > /etc/apache2/ports.conf && \
    echo "RemoteIPHeader X-Forwarded-For" >> /etc/apache2/apache2.conf && \
    echo "RemoteIPProxiesHeader X-Forwarded-By" >> /etc/apache2/apache2.conf && \
    echo "RemoteIPInternalProxy \${CLOUDRON_PROXY_IP}" >> /etc/apache2/apache2.conf && \
    # Enable required modules
    a2enmod rewrite headers remoteip && \
    # Lock www-data but allow su - www-data to work
    passwd -l www-data && usermod --shell /bin/bash --home /app/data www-data

# Add package config & scripts
COPY apache/glpi.conf /etc/apache2/sites-enabled/glpi.conf
ADD --chown=cloudron:cloudron pkg/ /app/pkg/

# Link key glpi directories for cloudron persistent storage
RUN mv -f /app/pkg/cloudroncommands /app/code/inc/console/cloudron && \
    rm -rf /app/code/.dependabot && \
    rm -rf /app/code/.github && \
    rm -f /app/code/*.md && \
    rm -f /app/code/install/install.php && \
    rm -rf /app/code/tests && \
    cp /app/code/config/.htaccess /app/code/bin/.htaccess && \
    cp /app/code/config/.htaccess /app/code/tools/.htaccess && \
    cp /app/code/config/.htaccess /app/code/vendor/.htaccess && \
    rm -rf /app/code/config && ln -sf /app/data/config /app/code/config && \
    rm -rf /app/code/files && ln -sf /app/data/files /app/code/files && \
    rm -rf /app/code/marketplace && ln -sf /app/data/marketplace /app/code/marketplace && \
    rm -rf /app/code/plugins && ln -sf /app/data/plugins /app/code/plugins && \
    rm -f /app/code/.htaccess && ln -sf /app/pkg/htaccess /app/code/.htaccess && \
    chown -R www-data:www-data /app/code

WORKDIR /app/data

CMD [ "/app/pkg/start.sh" ]
