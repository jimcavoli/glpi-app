#!/bin/bash

set -eu

mkdir -p /run/apache2 \
         /run/sessions \
         /app/data/config \
         /app/data/marketplace \
         /app/data/plugins \
         /app/data/files \
         /app/data/files/_cache \
         /app/data/files/_cron \
         /app/data/files/_dumps \
         /app/data/files/_graphs \
         /app/data/files/_locales \
         /app/data/files/_lock \
         /app/data/files/_log \
         /app/data/files/_pictures \
         /app/data/files/_plugins \
         /app/data/files/_rss \
         /app/data/files/_sessions \
         /app/data/files/_tmp \
         /app/data/files/_uploads

if [[ ! -f /app/data/.setup-complete ]]; then
  echo "==> Running first-time setup"

  # Write DB config and install intitial settings
  php /app/code/bin/console db:install --no-interaction --reconfigure --force \
    --default-language="en_US" \
    --db-host="${CLOUDRON_MYSQL_HOST}" \
    --db-port="${CLOUDRON_MYSQL_PORT}" \
    --db-name="${CLOUDRON_MYSQL_DATABASE}" \
    --db-user="${CLOUDRON_MYSQL_USERNAME}" \
    --db-password="${CLOUDRON_MYSQL_PASSWORD}"

  # Signal first-time setup complete
  touch /app/data/.setup-complete
fi

echo "=> Configuring database"
php /app/code/bin/console db:configure --no-interaction --reconfigure \
  --db-host="${CLOUDRON_MYSQL_HOST}" \
  --db-port="${CLOUDRON_MYSQL_PORT}" \
  --db-name="${CLOUDRON_MYSQL_DATABASE}" \
  --db-user="${CLOUDRON_MYSQL_USERNAME}" \
  --db-password="${CLOUDRON_MYSQL_PASSWORD}"

echo "=> Migrating database"
php /app/code/bin/console db:update --no-interaction

echo "=> Configuring GLPI"
cat <<HTACCESS | tee /app/data/config/.htaccess /app/data/files/.htaccess > /dev/null
<IfModule mod_authz_core.c>
Require all denied
</IfModule>
<IfModule !mod_authz_core.c>
deny from all
</IfModule>
HTACCESS
/app/code/bin/console cloudron:config
/app/code/bin/console cloudron:ldap

if [[ ! -d /app/data/inc ]]; then
  # symlink inc dir into /app/data to protect plugins with relative directory navigation
  ln -sfn /app/code/inc /app/data/inc
fi

echo "=> Changing permissions"
chown -R www-data:www-data /app/data /run

APACHE_CONFDIR="" source /etc/apache2/envvars
rm -f "${APACHE_PID_FILE}"
exec /usr/sbin/apache2 -DFOREGROUND
