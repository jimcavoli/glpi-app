<?php
namespace Glpi\Console\Cloudron;

if (!defined('GLPI_ROOT')) {
   die("Sorry. You can't access this file directly");
}

use AuthLDAP;
use Glpi\Console\AbstractCommand;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CloudronLdapCommand extends AbstractCommand {

   protected function configure() {

      global $CFG_GLPI;

      parent::configure();

      $this->setName('glpi:cloudron:ldap');
      $this->setAliases(['cloudron:ldap']);
      $this->setDescription(__('Maintains (add/update) configuration for the Cloudron LDAP server.'));

   }

   protected function execute(InputInterface $input, OutputInterface $output) {

      global $CFG_GLPI;

      $cloudron_ldap_new = [
         'name'               => 'Cloudron LDAP',
         'is_active'          => 1,
         'is_default'         => 1
      ];

      $cloudron_ldap_config = [
        'host'               => getenv('CLOUDRON_LDAP_SERVER'),
        'basedn'             => getenv('CLOUDRON_LDAP_USERS_BASE_DN'),
        'rootdn'             => getenv('CLOUDRON_LDAP_BIND_DN'),
        'port'               => getenv('CLOUDRON_LDAP_PORT'),
        'rootdn_passwd'      => getenv('CLOUDRON_LDAP_BIND_PASSWORD'),
        'login_field'        => 'username',
        'sync_field'         => 'uid',
        'email1_field'       => 'mail',
        'realname_field'     => 'sn',
        'firstname_field'    => 'givenname',
        'phone_field'        => '',
        'email2_field'       => 'mailalternateaddress',
        'group_field'        => 'memberof'
      ];

      $servers = \AuthLDAP::getLdapServers();
      $ldap = new \AuthLDAP();

      foreach($servers as $server) {
         if ($server['host'] == getenv('CLOUDRON_LDAP_SERVER')) {
            $ldap -> getFromDB($server['id']);
            $ldap -> update(array_merge($server, $cloudron_ldap_config));
            return 0;
         }
      }

      $ldap -> add(array_merge($cloudron_ldap_config, $cloudron_ldap_new));

      return 0; // Success
   }
}
