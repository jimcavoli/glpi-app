<?php
namespace Glpi\Console\Cloudron;

if (!defined('GLPI_ROOT')) {
   die("Sorry. You can't access this file directly");
}

use \Config;
use Glpi\Console\AbstractCommand;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CloudronConfigCommand extends AbstractCommand {

   protected function configure() {

      global $CFG_GLPI;

      parent::configure();

      $this->setName('glpi:cloudron:config');
      $this->setAliases(['cloudron:config']);
      $this->setDescription(__('Maintains configuration needed to keep GLPI configured properly for the Cloudron server.'));

   }

   protected function execute(InputInterface $input, OutputInterface $output) {

      global $CFG_GLPI;

      $cloudron_core_configs = [
         'url_base'      => getenv('CLOUDRON_APP_ORIGIN'),
         'url_base_api'  => getenv('CLOUDRON_APP_ORIGIN') . '/api/',
         // SMTP Mode 1 is plain SMTP (no ssl/tls)
         'smtp_mode'     => 1,
         'smtp_host'     => getenv('CLOUDRON_MAIL_SMTP_SERVER'),
         'smtp_port'     => getenv('CLOUDRON_MAIL_SMTP_PORT'),
         'smtp_username' => getenv('CLOUDRON_MAIL_SMTP_USERNAME'),
         // TODO: remove Toolbox::sodiumEncrypt call wrapping value in next version (fixed after 9.5.2)
         'smtp_passwd'   => \Toolbox::sodiumEncrypt(getenv('CLOUDRON_MAIL_SMTP_PASSWORD')),
         'smtp_sender'   => getenv('CLOUDRON_MAIL_FROM')
      ];

      \Config::setConfigurationValues('core', $cloudron_core_configs);

      return 0; // Success
   }
}
